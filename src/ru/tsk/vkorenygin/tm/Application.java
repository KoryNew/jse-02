package ru.tsk.vkorenygin.tm;

import ru.tsk.vkorenygin.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("--- Welcome to Task Manager ---");
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (TerminalConst.CMD_ABOUT.equals(arg)) showAbout();
        if (TerminalConst.CMD_VERSION.equals(arg)) showVersion();
        if (TerminalConst.CMD_HELP.equals(arg)) showHelp();
    }

    public static void showAbout() {
        System.out.println("-- ABOUT --");
        System.out.println("Developed by: Vladimir Korenyugin");
        System.out.println("E-mail: vkorenygin@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("-- VERSION --");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("-- HELP --");
        System.out.println("about - displays developer info");
        System.out.println("version - displays program version");
        System.out.println("help - displays list of commands");
    }
}
